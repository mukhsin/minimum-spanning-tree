Array.prototype.contains = function(element){
    return this.indexOf(element) > -1;
};

function getRandom(start, end) {
    return Math.floor(Math.random() * end) + start;
}

function getXFromTree(tree) {
    var x = [];
    for (var i = 0; i < tree.length; i++) {
        x.push(tree[i].x);
    }
    return x;
}

function getYFromTree(tree) {
    var y = [];
    for (var i = 0; i < tree.length; i++) {
        y.push(tree[i].y);
    }
    return y;
}

function getIndexFromTree(tree) {
    var index = [];
    for (var i = 0; i < tree.length; i++) {
        index.push(tree[i].index);
    }
    return index;
}

function getJarak(node1, node2) {
    var dx = Math.abs(node1.x - node2.x);
    var dy = Math.abs(node1.y - node2.y);
    var jarak = Math.sqrt(dx * dx + dy * dy);
    return Math.round(jarak * 100) / 100;
}

function getRandomPrufer(length) {
    var prufer = [];
    for (var i = 0; i < length - 2; i++) {
        prufer.push(getRandom(0, length));
    }
    return prufer;
}

function getPruferDraw(tree, prufer) {
    var graph = [];
    var newTree = tree.slice();
    var i = 0;

    while (newTree.length > 2) {
        if (!prufer.contains(newTree[i].index)) {
            // console.log(newTree.length, i, prufer, prufer[0], graph, getIndexFromTree(newTree));
            var pruferLabel = tree[prufer[0]].label;
            var nodeLabel = newTree[i].label;
            graph.push(prufer[0] + '-' + newTree[i].index);

            newTree.splice(i, 1);
            prufer.splice(0, 1);
            i = 0;
        } else {
            i++;
        }
    }

    graph.push(newTree[0].index + '-' + newTree[1].index);
    // console.log(newTree.length, i, prufer, graph, getIndexFromTree(newTree));

    return graph;
}

function getBobot(tree, pruferDraw) {
    var bobot = 0;
    for (var i = 0; i < pruferDraw.length; i++) {
        var node1 = tree[parseInt(pruferDraw[i].split('-')[0])];
        var node2 = tree[parseInt(pruferDraw[i].split('-')[1])];
        bobot += getJarak(node1, node2);
    }
    return Math.round(bobot * 100) / 100
}
